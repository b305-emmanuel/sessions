//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Calculating Cube and Printing with Template Literals:
const getCube = (number) => number ** 3;

const num = 2;
const cube = getCube(num);

console.log(`The cube of ${num} is ${cube}`);



// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

// Exponent Operator

// Template Literals


// 
const address = ["Mother Ignacia Ave.", "South Triangle", "Diliman", "Quezon City"];

const [street, city, state, zip] = address;

console.log(`I live at ${street}, ${city}, ${state}, ${zip}`);

// 
const animal = {
    name: "Lolong",
    species: "salt water crocodile",
    weight: 1075,
    length: {
      feet: 20,
      inches: 3,
    },
  };
  
  const { name, species, weight, length } = animal;
  const { feet, inches } = length;
  
  console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${feet} ft ${inches} in.`);
  

// Array Destructuring
// const address = ["258", "Washington Ave NW", "California", "90011"];

// // Object Destructuring
// const animal = {
//     name: "Lolong",
//     species: "saltwater crocodile",
//     weight: "1075 kgs",
//     measurement: "20 ft 3 in"
// }


// Arrow Functions
// let numbers = [1, 2, 3, 4, 5];

// 

const numbers = [1, 2, 3, 4, 5];

// Using forEach and implicit return to print numbers
numbers.forEach((number) => console.log(number));

// Using reduce to calculate the sum of all numbers
const reduceNumber = numbers.reduce((sum, number) => sum + number, 0);

console.log(reduceNumber);

// Javascript Classes
// 
class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}
