//Functions
    
//Parameters and Arguments

// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
// Functions are mostly created to create complicated tasks to run several lines of code in succession
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

//We also learned in the previous session that we can gather data from user input using a prompt() window.


function printInput(){

    let nickname = prompt("Enter your nickname:");
    console.log("Hi, " + nickname);

}

printInput();



//However, for some use cases, this may not be ideal. 
//For other cases, functions can also process data directly passed into it instead of relying only on Global Variables and prompt().

//Consider this function:

function printName(name){

    console.log("My name is " + name);

};

printName("Juana");



//You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

// "name" is called a parameter
// A "parameter" acts as a named variable/container that exists only inside of a function
// It is used to store information that is provided to a function when it is called/invoked.

//"Juana", the information/data provided directly into the function is called an argument.
//Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

//In the following examples, "John" and "Jane" are both arguments since both of them are supplied as information that will be used to print out the full message.

// When the "printName()" function is first called, it stores the value of "John" in the parameter "name" then uses it to print a message.
printName("John");

// When the "printName()" function is called again, it stores the value of "Jane" in the parameter "name" then uses it to print a message.
printName("Jane");

//variables can also be passed as an argument.
let sampleVariable = "Yui";

printName(sampleVariable); 

//Function arguments cannot be used by a function if there are no parameters provided within the function.

//Now, you can create a function which can be re-used to check for a number's divisibility instead of having to manually do it every time like our previous activity!

function checkDivisibilityBy8(num){
let remainder = num % 8;
console.log("The remainder of " + num + " divided by 8 is: " + remainder);
let isDivisibleBy8 = remainder === 0;
console.log("Is " + num + " divisible by 8?");
console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

//You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

// Functions as Arguments

// Function parameters can also accept other functions as arguments
// Some complex functions use other functions as arguments to perform more complicated results
// This will be further seen when we discuss array methods.




function argumentFunction(){
console.log("This function was passed as an argument before the message was printed.")
}

function invokeFunction(argumentFunction){
argumentFunction();
}


// Adding and removing the parentheses "()" impacts the output of JavaScript heavily
// When a function is used with parentheses "()", it denotes invoking/calling a function
// A function used without a parenthesis is normally associated with using the function as an argument to another function
invokeFunction(argumentFunction);

//or finding more information about a function in the console using console.log()
console.log(argumentFunction);

//Using multiple parameters

// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.



function createFullName(firstName, middleName, lastName) {

console.log(firstName + ' ' + middleName + ' ' + lastName);

}

createFullName('Juan', 'Dela', 'Cruz');

// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"


// In JavaScript, providing more/less arguments than the expected parameters will not return an error.

//Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.

// In other programming languages, this will return an error stating that "the expected number of arguments do not match the number of parameters".


createFullName('Juan', 'Dela');
createFullName('Jane', 'Dela', 'Cruz', 'Hello');


// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

//Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.


function printFullName(middleName,firstName,lastName){

console.log(firstName + ' ' + middleName + ' ' + lastName);

}

printFullName('Juan', 'Dela', 'Cruz');

//results to "Dela Juan Cruz" because "Juan" was received as middleName, "Dela" was received as firstName.

// Using alert()

//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

alert("Hello World!");//This will run immediately when the page loads.

//alert() syntax:
//alert("<messageInString>");


//You can use an alert() to show a message to the user from a later function invocation.
function showSampleAlert(){
    alert("Hello, User!");
}

showSampleAlert();

//You will find that the page waits for the user to dismiss the dialog before proceeding. You can witness this by reloading the page while the console is open.

console.log("I will only log in the console when the alert is dismissed.");

//Notes on the use of alert():
    //Show only an alert() for short dialogs/messages to the user. 
    //Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

//Using prompt()

//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

let samplePrompt = prompt("Enter your Name.");

//console.log(typeof samplePrompt);//The value of the user input from a prompt is returned as a string.
console.log("Hello, " + samplePrompt);

/*
    prompt() Syntax:

    prompt("<dialogInString>");

*/

let sampleNullPrompt = prompt("Don't enter anything.");

console.log(sampleNullPrompt);//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

//Let's create function scoped variables to store the returned data from our prompt(). This way, we can dictate when to use a prompt() window or have a reusable function to use our prompts.
function printWelcomeMessage(){
    let firstName = prompt("Enter Your First Name");
    let lastName = prompt("Enter Your Last Name");

    console.log("Hello, " + firstName + " " + lastName + "!");
    console.log("Welcome to my page!");
}

printWelcomeMessage();

function greeting(){
console.log("Hi, this is me");
return "Hello, This is the return statement";
// code below return statement will never work
// return -> breaking statement of the function
console.log("Hello this is me");
}


let greetingFunction = greeting();
console.log(greetingFunction);

console.log(greeting());


