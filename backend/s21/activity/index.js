/*
    1. Create a function named getUserInfo which is able to return an object. 

        The object returned should have the following properties:
        
        - key - data type

        - name - String
        - age -  Number
        - address - String
        - isMarried - Boolean
        - petName - String

        Note: Property names given is required and should not be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.

*/

function getUserInfo() {
  return {
    name: 'Bill Emmanuel',
    age: 23,
    address: 'Quezon City',
    isMarried: false,
    petName: 'Jessica'
  };
}

const userInfo = getUserInfo();
console.log(userInfo);



/*
  2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
      
      - Note: the array returned should have at least 5 elements as strings.
              function name given is required and cannot be changed.


      To check, create a variable to save the value returned by the function.
      Then log the variable in the console.

      Note: This is optional.
  
*/function getArtistsArray() {
  return ['Eminem', 'SB19', 'Dr Dre', 'Neffex', 'Avicii'];
}

console.log(getArtistsArray);

/*
  3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

      - Note: the array returned should have at least 5 elements as strings.
              function name given is required and cannot be changed.

      To check, create a variable to save the value returned by the function.
      Then log the variable in the console.

      Note: This is optional.
*/

function getSongsArray() {
  return ['Till I Collapse', 'Bazinga', 'Still D.R.E', 'Fight Back', 'The Nights']
}

console.log(getSongsArray);


/*
  4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

      - Note: the array returned should have at least 5 elements as strings.
              function name given is required and cannot be changed.

      To check, create a variable to save the value returned by the function.
      Then log the variable in the console.

      Note: This is optional.
*/

function getMoviesArray() {
  return ['Iron Man', 'Iron Man 2', 'The Avengers', 'Avengers: Age of Ultron', 'Avengers: Endgame']
}

console.log(getMoviesArray);




/*
  5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

          - Note: the array returned should have numbers only.
                  function name given is required and cannot be changed.

          To check, create a variable to save the value returned by the function.
          Then log the variable in the console.

          Note: This is optional.
          
*/

function isPrime(num) {
  if (num <= 1) return false;
  if (num <= 3) return true;

  if (num % 2 === 0 || num % 3 === 0) return false;

  for (let i = 5; i * i <= num; i += 6) {
    if (num % i === 0 || num % (i + 2) === 0) return false;
  }

  return true;
}

function getPrimeNumberArray() {
  const primeArray = [];
  let num = 2;
  while (primeArray.length < 5) {
    if (isPrime(num)) {
      primeArray.push(num);
    }
    num++;
  }
  return primeArray;
}

const primeNumbers = getPrimeNumberArray();
console.log(primeNumbers);