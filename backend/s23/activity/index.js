// /*
// 2
//     1. Create a login function which is able to receive 3 parameters called username,password and role.
// 3
//         -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.
// 4
//             -if it is, return a message in console to inform the user that their input should not be empty.
// 5
//         -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
// 6
//                 -if the user's role is admin, return the following message:
// 7
//                     "Welcome back to the class portal, admin!"
// 8
//                 -if the user's role is teacher,return the following message:
// 9
//                     "Thank you for logging in, teacher!"
// 10
//                 -if the user's role is a rookie,return the following message:
// 11
//                     "Welcome to the class portal, student!"
// 12
//                 -if the user's role does not fall under any of the cases, as a default, return a message:
// 13
//                     "Role out of range."

// Function to handle user login
function login(username, password, role) {
  if (!username || !password || !role) {
    return 'Inputs must not be empty';
  } else {
    switch (role) {
      case 'admin':
        return 'Welcome back to the class portal, admin!';
      case 'teacher':
        return 'Thank you for logging in, teacher!';
      case 'rookie':
        return 'Welcome to the class portal, student!';
      default:
        return 'Role out of range.';
    }
  }
}

// Function to gather user input and simulate login
function simulateLogin() {
  const username = prompt('Enter username:');
  const password = prompt('Enter password:');
  const role = prompt('Enter role (admin, teacher, rookie):');

  const loginResult = login(username, password, role);
  console.log(loginResult);
}

// Simulate the login process
simulateLogin();

// 14
// */

// /*
// 16
//     2. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
// 17
//         -add parameters appropriate to describe the arguments.
// 18
//         -create a new function scoped variable called average.
// 19
//         -calculate the average of the 4 number inputs and store it in the variable average.
// 20
//         -research the use of Math.round() and round off the value of the average variable.
// 21
//             -update the average variable with the use of Math.round()
// 22
//              -Do not use Math.floor().
// 23
//             -console.log() the average variable to check if it is rounding off first.
// 24
//         -add an if statement to check if the value of average is less than or equal to 74.
// 25
//             -if it is, return the following message:
// 26
//             "Hello, student, your average is <show average>. The letter equivalent is F"
// 27
//         -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
// 28
//             -if it is, return the following message:
// 29
//             "Hello, student, your average is <show average>. The letter equivalent is D"
// 30
//         -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
// 31
//             -if it is, return the following message:
// 32
//             "Hello, student, your average is <show average>. The letter equivalent is C"
// 33
//         -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
// 34
//             -if it is, return the following message:
// 35
//             "Hello, student, your average is <show average>. The letter equivalent is B"
// 36
//         -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
// 37
//             -if it is, return the following message:
// 38
//             "Hello, student, your average is <show average>. The letter equivalent is A"
// 39
//         -add an else if statement to check if the value of average is greater than 96.
// 40
//             -if it is, return the following: message:
// 41
//             "Hello, student, your average is <show average>. The letter equivalent is A+"

// Function to handle user login// Function to handle user login







function gatherGrades() {
  const grades = [];
  for (let i = 1; i <= 4; i++) {
      const grade = parseFloat(prompt(`Enter grade ${i}:`));
      grades.push(grade);
  }
  return grades;
}

function checkAverage(grades) {
  const average = Math.round(grades.reduce((sum, grade) => sum + grade, 0) / grades.length);

  let message = `Hello, student, your average is: ${average}.`;

  if (average <= 74) {
      message += " The letter equivalent is F";
  } else if (average >= 75 && average <= 79) {
      message += " The letter equivalent is D";
  } else if (average >= 80 && average <= 84) {
      message += " The letter equivalent is C";
  } else if (average >= 85 && average <= 89) {
      message += " The letter equivalent is B";
  } else if (average >= 90 && average <= 95) {
      message += " The letter equivalent is A";
  } else {
      message += " The letter equivalent is A+";
  }

  return message;
}

const grades = gatherGrades();
const result = checkAverage(grades);
console.log(result);