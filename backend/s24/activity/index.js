// Prompt user for a number
var number = parseInt(prompt("Enter a number:"));

// Loop for divisibility by 5 and conditions
for (var i = number; i > 0; i--) {
  if (i <= 50) {
    console.log("The current value is at 50. Terminating the loop.");
    break;
  } else if (i % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
  } else if (i % 5 === 0) {
    console.log(i);
  }
}

// String operations
var string = "supercalifragilisticexpialidocious";
var filteredString = "";

// Loop to filter consonants
for (var j = 0; j < string.length; j++) {
  var letter = string[j];
  if (/[aeiouAEIOU]/.test(letter)) {
    continue;
  } else {
    filteredString += letter;
  }
}

// Print results
console.log(filteredString);