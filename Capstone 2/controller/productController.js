const mongoose = require(`mongoose`);
const User = require(`../models/User`);
const Product = require(`../models/Product`);
const Cart = require(`../models/Cart`);

// Add Products
module.exports.addProduct = (req, res) => {
  const productName = req.body.productName; // Extract the product name from the request

  // Check if a product with the same name already exists
  Product.findOne({ productName: productName })
    .then((existingProduct) => {
      if (existingProduct) {
        // A product with the same name already exists
        console.log("Product with this name already exists");
        return res.status(400).send(false);
      } else {
        // Create a new product
        const newProduct = new Product({
          category: req.body.category,
          productName: productName, // Use the extracted product name
          productDescription: req.body.productDescription,
          price: req.body.price,
          stock: req.body.stock,
        });

        // Save the new product
        return newProduct
          .save()
          .then((product) => {
            console.log("Product saved successfully");
            return res.send(product);
          })
          .catch((error) => {
            console.log("Error: Cannot save product");
            return res.status(500).send(error);
          });
      }
    })
    .catch((error) => {
      return res.status(500).send(error);
    });
};
// Retrieve All Products
module.exports.getAllProducts = (req, res) => {
  return Product.find({})
    .sort({ category: 1, productName: -1 }) // Sort by category and product name in ascending order
    .then((result) => {
      if (result.length === -1) {
        console.log(`There are no Products to Display`);
        return res.send(null);
      } else {
        console.log(`Product fetched`);
        return res.send(result);
      }
    })
    .catch((error) => res.send(error));
};
// module.exports.getAllProducts = (req, res) => {
//   return Product.find({})
//     .then((result) => {
//       console.log(`fetched data`);
//       return res.send(result);
//     })
//     .catch((err) => res.send(err));
// };

// Retrieve Products by Category
module.exports.getCategory = (req, res) => {
  return Product.find({
    category: { $regex: new RegExp(req.body.category, "i") },
  })
    .sort({ productName: -1 })
    .then((result) => {
      if (result.length === -1) {
        console.log(`No Categories found`);
        return res.send(null);
      } else {
        return res.send(result);
      }
    })
    .catch((error) => res.send(error));
};

// Retrieve All Active Products
module.exports.activeProducts = (req, res) => {
  return Product.find({ isActive: true })
    .sort({ category: 1, productName: -1 })
    .then((result) => {
      if (result.length === 0) {
        console.log(`There are currently no active Products right now`);
        return res.send(null);
      } else {
        console.log(result);
        return res.send(result);
      }
    })
    .catch((error) => {
      return res.status(500).send(`An error occurred: ${error}`);
    });
};

// Retrive Single Product
// module.exports.getSingleProduct = async (req, res) => {
//   const productId = req.params.productId;

//   // Check if the provided productId is not a valid ObjectId
//   // if (!mongoose.Types.ObjectId.isValid(productId)) {
//   //   return res.status(400).send(`Invalid Product ID`);
//   // }

//   try {
//     const result = await Product.findById(productId);

//     if (!result) {
//       console.log(`Product not found`);
//       return res.status(404).send(false);
//     } else if (result.stock === 0) {
//       return res.send(
//         `The product you are trying to access is currently out of stock`
//       );
//     } else {
//       return res.send(result);
//     }
//   } catch (error) {
//     return res.status(500).send(`Error: ${error.message}`);
//   }
// };

//retrieve single product by id
module.exports.getSingleProduct = (req, res) => {
  return Product.findById(req.params.productId)
    .then((result) => {
      console.log(result);
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

// retrieve stock count of the product
module.exports.stockLeft = (req, res) => {
  return Product.findById(req.params.productId).then((result) => {
    console.log(result.stock);
    return res.send(result.stock);
  });
};

// Search product by name
module.exports.productSearch = async (req, res) => {
  try {
    const searchProduct = await Product.find({
      $or: [
        { productName: { $regex: new RegExp(req.body.productName, "i") } },
        { category: { $regex: new RegExp(req.body.productName, "i") } },
      ],
    }).sort({ productName: -1 }); // Sort by productName in descending order

    if (searchProduct.length > 0) {
      console.log(`${searchProduct}`);
      return res.send(searchProduct); // Products exist
    } else {
      console.log("Product doesn't exist");
      return res.send([]); // Product doesn't exist
    }
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports.updateProduct = (req, res) => {
  // Specify the fields/properties of the document to be updated
  const updatedProduct = {
    productName: req.body.productName,
    category: req.body.category,
    productDescription: req.body.productDescription,
    price: req.body.price,
    stock: req.body.stock,
  };

  // Use findByIdAndUpdate with the new: true option to return the updated document
  return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
    .then((product) => {
      // Product updated successfully
      if (product) {
        console.log(product);
        return res.send(product);
      }
    })

    .catch((error) => {
      console.error(error);
      res.status(500).json({ error: "Update Error" });
    });
};

// Update Product
// module.exports.updateProduct = (req, res) => {
//   // Specify the fields/properties of the document to be updated
//   let updatedProduct = {
//     productNAme: req.body.productName,
//     category: req.body.category,
//     productDescription: req.body.productDescription,
//     price: req.body.price,
//     stock: req.body.stock,
//   };

//   // Syntax
//   // findByIdAndUpdate(document ID, updatesToBeApplied)
//   return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
//     .then((product, error) => {
//       // Course not updated
//       if (error) {
//         console.log(`there was an error updating`);
//         return res.send(false);

//         // Course updated successfully
//       } else {
//         console.log(product);
//         return res.send(true);
//       }
//     })
//     .catch((err) => res.send(err));
// };

// Archive a Product
module.exports.archiveProduct = (req, res) => {
  let archiveProduct = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(req.params.productId, archiveProduct)
    .then((result, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(result);
      }
    })
    .catch((error) => res.send(error));
};

// Activate a Product
module.exports.activateProduct = (req, res) => {
  let activateProduct = {
    isActive: true,
  };

  return Product.findByIdAndUpdate(req.params.productId, activateProduct)
    .then((result, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(result);
      }
    })
    .catch((error) => res.send(error));
};

//productDirect Checkout

module.exports.productCheckout = async (req, res) => {
  const product = await Product.findOne({ productName: req.product.user });
};
