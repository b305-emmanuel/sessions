const User = require(`../models/User`);
const Product = require(`../models/Product`);
const Cart = require(`../models/Cart`);
const bcrypt = require(`bcrypt`);
const auth = require(`../auth`);

// Register User
module.exports.registerUser = async (req, res) => {
  try {
    // Check if the password contains at least one capital letter and one number
    const passwordRegex = /^(?=.*[A-Z])(?=.*\d)/;
    if (!passwordRegex.test(req.body.password)) {
      console.log(
        `Password must contain at least one Capital letter and One number.`
      );
      return res.send(false);
    }

    // If email is unique and password meets the requirements, proceed to create a new user
    const newUser = new User({
      fullName: req.body.fullName,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 10),
      mobileNo: req.body.mobileNo,
    });

    await newUser.save();
    console.log(`Registration successful`);
    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.send(false);
  }
};

// Check email if existing
module.exports.checkEmail = async (req, res) => {
  try {
    // Check if the email already exists in the database
    const existingUser = await User.findOne({ email: req.body.email });

    if (existingUser) {
      console.log(`Email already exists.`);
      return res.status(200).json({ exists: true });
    }

    // If the email doesn't exist, you can return exists: false or simply a success status
    console.log(`email does not exist`);
    return res.status(200).json({ exists: false });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
};

// Login User
module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email }).then((result) => {
    if (result === null) {
      return res.send(`User doesn't Exists.`);
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );
      console.log(isPasswordCorrect);

      if (isPasswordCorrect) {
        return res.send({ access: auth.createAccessToken(result) });
      } else {
        return res.send(`Incorrect Password.`);
      }
    }
  });
};

module.exports.getProfile = (req, res) => {
  return User.findById(req.user.id)
    .then((result) => {
      result.password = "";
      return res.send(result);
    })
    .catch((err) => res.send(err));
};
// make a User as admin
module.exports.activateAdmin = (req, res) => {
  let activateAdmin = {
    isAdmin: true,
  };

  return User.findByIdAndUpdate(req.params.userId, activateAdmin)
    .then((result) => {
      if (result) {
        return res.send(`Successfully Changed as a admin`);
      } else {
        return res.send(false);
      }
    })
    .catch((error) => res.send(error));
};

// Make a admin as a User
module.exports.deactivateAdmin = (req, res) => {
  let deactivateAdmin = {
    isAdmin: false,
  };

  return User.findByIdAndUpdate(req.params.userId, deactivateAdmin)
    .then((result) => {
      if (result) {
        return res.send(`Successfully Changed as a User`);
      } else {
        return res.send(false);
      }
    })
    .catch((error) => res.send(error));
};

// Retrieve all User
module.exports.getAllUser = (req, res) => {
  return User.find({})
    .sort({ userName: 1 })
    .select("fullName email mobileNo isActive isAdmin ")
    .then((result) => res.send(result));
};

// Retrieve One User by id
module.exports.getUser = (req, res) => {
  return User.findById(req.user.id)
    .then((result) => {
      result.password = "";
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

// Update User
module.exports.updateUser = async (req, res) => {
  try {
    // Check if the email already exists in the database, excluding the current user's email
    const existingUser = await User.findOne({
      email: req.body.email,
      _id: { $ne: req.user.id },
    });

    if (existingUser) {
      console.log(`Email already exists.`);
      return res.send(false);
    }

    // Check if the password contains at least one capital letter and one number (if provided)
    if (req.body.password) {
      const passwordRegex = /^(?=.*[A-Z])(?=.*\d)/;
      if (!passwordRegex.test(req.body.password)) {
        console.log(
          `Password must contain at least one capital letter and one number.`
        );
        return res.send(false);
      }
    }

    // Update user data
    const updateUser = {
      fullName: req.body.fullName,
      email: req.body.email,
      mobileNo: req.body.mobileNo,
      password: req.body.password,
    };

    // If a new password is provided, hash it and update the user's password
    if (req.body.password) {
      updateUser.password = bcrypt.hashSync(req.body.password, 10);
    }

    // Find and update the user
    const updatedUser = await User.findByIdAndUpdate(req.user.id, updateUser, {
      new: true,
    });

    if (updatedUser) {
      return res.send(updatedUser);
    } else {
      console.log(`user not found`);
      return res.send(false); // User not found
    }
  } catch (error) {
    console.error(error);
    return res.send(error);
  }
};

// Reset Password
module.exports.resetPassword = async (req, res) => {
  try {
    const newPassword = req.body.newPassword;
    const userId = req.user.id;

    if (newPassword) {
      // Check the new password
      const passwordRegex = /^(?=.*[A-Z])(?=.*\d)/;
      if (!passwordRegex.test(newPassword)) {
        return res.send(
          `Password must contain at least one capital letter and one number.`
        );
      }
    }

    // Hash the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await User.findByIdAndUpdate(userId, { password: hashedPassword });

    res.status(200).json({ message: "Password Reset successfully" });
  } catch (error) {
    res.status(500).json({ message: `Internal Server Error` });
  }
};
