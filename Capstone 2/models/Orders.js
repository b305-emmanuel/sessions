const mongoose = require(`mongoose`);

const orderSchema = new mongoose.Schema({
  activeOrders: [
    {
      userId: String,
      userName: {
        type: String,
        required: [true, "User Name is Required"],
      },
      isActive: {
        type: String,
        default: true,
      },
      items: [
        {
          productId: String,
          productName: String,
          price: {
            type: Number,
            default: 0,
          },
          quantity: Number,
          totalPrice: Number,
        },
      ],
      totalCartPrice: {
        type: Number,
        default: 0,
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
  processedOrders: [
    {
      userId: String,
      userName: {
        type: String,
        required: [true, "User Name is Required"],
      },
      isActive: {
        type: String,
        default: false,
      },
      items: [
        {
          productId: String,
          productName: String,
          price: {
            type: Number,
            default: 0,
          },
          quantity: Number,
          totalPrice: Number,
        },
      ],
      totalCartPrice: {
        type: Number,
        default: 0,
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

// Function to calculate the total price for each item
orderSchema.methods.calculateTotalPrice = function () {
  this.items.forEach((item) => {
    item.totalPrice = item.quantity * item.price;
  });
};

// Middleware to calculate the total cart price for items before saving
orderSchema.pre("save", function (next) {
  this.calculateTotalPrice();
  this.totalCartPrice = this.items.reduce(
    (total, item) => total + item.totalPrice,
    0
  );
  next();
});

module.exports = mongoose.model(`Orders`, orderSchema);
