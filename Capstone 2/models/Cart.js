const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    // required: [true, "User ID is required"],
  },
  userName: String, // Add a field for user name
  items: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required"],
      },
      productName: String, // Include other product details as needed
      quantity: {
        type: Number,
        required: [true, "Product quantity is required"],
        default: 1, // You can set a default value if needed
      },
      stock: {
        type: Number,
      },
      price: {
        type: Number,
        required: [true, "Product Price is required"],
      },
      totalPrice: {
        type: Number,
        default: 0, // Default to 0, as it will be calculated
      },
      // Add more product details here
    },
  ],
  totalCartPrice: {
    type: Number,
    default: 0, // Default to 0, as it will be calculated
  },
});

// Function to calculate the total price for each item
cartSchema.methods.calculateTotalPrice = function () {
  this.items.forEach((item) => {
    item.totalPrice = item.quantity * item.price;
  });
};

// Middleware to calculate the total cart price for items before saving
cartSchema.pre("save", function (next) {
  this.calculateTotalPrice();
  this.totalCartPrice = this.items.reduce(
    (total, item) => total + item.totalPrice,
    0
  );
  next();
});

module.exports = mongoose.model("Cart", cartSchema);
