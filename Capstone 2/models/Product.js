const mongoose = require("mongoose");

// Define the product schema
const productSchema = new mongoose.Schema({
  category: {
    type: String,
    required: [true, `Category is required`],
  },
  productName: {
    type: String,
    required: [true, `Product Name is required`],
  },
  productDescription: {
    type: String,
  },
  price: {
    type: Number,
    required: [true, `Price is required`],
  },
  stock: {
    type: Number,
  },
  deliveredOn: {
    type: Date,
    default: new Date(),
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

// Create models for both schemas
module.exports = mongoose.model("Product", productSchema);
