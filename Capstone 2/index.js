const express = require(`express`);
const mongoose = require(`mongoose`);
const cors = require(`cors`);

require(`dotenv`).config();

// Allows access to routes defined within our app
const userRoutes = require(`./routes/userRoutes`);
const productRoutes = require(`./routes/productRoutes`);
const cartRoutes = require(`./routes/cartRoutes`);
const orderRoutes = require(`./routes/ordersRoutes`);

// [SECTION] Environment Setup
const port = 4000;

//[SECTION] server setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Allows all resources to access our backend application
app.use(cors());

// Database Connection
mongoose.connect(
  "mongodb+srv://admin:admin123@cluster0.uafxxaa.mongodb.net/capstone-2?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // allows us to avoid any current or future errors while connecting to mongoDB
  }
);

mongoose.connection.once(`open`, () =>
  console.log(`We're connected to the cloud database.`)
);

// [SECTION] Backend Routes
app.use(`/users`, userRoutes);
app.use(`/products`, productRoutes);
app.use(`/cart`, cartRoutes);
app.use(`/orders`, orderRoutes);

//  [SECTION] Server Gateway Response
if (require.main === module) {
  app.listen(port, () => {
    console.log(`API is now online on port ${port} `);
  });
}

module.exports = app;
