let collection = [];

function print() {
  return collection;
}

function enqueue(item) {
  // Add an item to the back of the queue
  collection.push(item);
  return collection;
}

function dequeue() {
  // Remove an item from the front of the queue
  collection.shift();
  return collection;
}

function front() {
  // Get the front item of the queue
  return collection[0];
}

function size() {
  // Get the size of the queue
  return collection.length;
}

function isEmpty() {
  // Check if the queue is empty
  return collection.length === 0;
}

// Export create queue functions below.
module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};