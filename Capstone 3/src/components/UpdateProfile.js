import React, { useState } from "react";

const UpdateProfileForm = () => {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState(``);
  const [mobileNo, setMobileNo] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    const token = localStorage.getItem("token");

    const profileData = {
      fullName,
      email,
      mobileNo,
    };

    fetch("https://capstone-2-emg9.onrender.com/users/profile", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(profileData),
    })
      .then((res) => res.json())
      .then((response) => {
        if (response._id) {
          // Profile update was successful, refresh the page
          window.location.reload();
        } else {
          throw new Error("Profile update failed");
        }
      })
      .catch((error) => {
        console.error(error);
        // Handle error here
      });
  };

  return (
    <div className="container">
      <h2 className="my-4">Update Profile</h2>
      <form onSubmit={(e) => handleSubmit(e)}>
        <div className="mb-3">
          <label htmlFor="firstName" className="form-label">
            Full Name
          </label>
          <input
            type="text"
            className="form-control"
            id="fullName"
            value={fullName}
            onChange={(e) => setFullName(e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="lastName" className="form-label">
            Email
          </label>
          <input
            type="text"
            className="form-control"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="mobileNo" className="form-label">
            Mobile Number
          </label>
          <input
            type="text"
            className="form-control"
            id="mobileNo"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Update Profile
        </button>
      </form>
    </div>
  );
};

export default UpdateProfileForm;
