import React, { useState, useEffect } from "react";
import ProductCard from "./ProductCard";
import ProductSearch from "./ProductSearch";

export default function UserView({ productData }) {
  const [product, setProduct] = useState([]);

  useEffect(() => {
    const productArr = productData.map((product) => {
      if (product.isActive === true || product.isActive === "true") {
        console.log(product.isActive);
        return (
          <>
            <ProductCard productProp={product} key={product._id} />
          </>
        );
      } else {
        return <h1>No Results</h1>;
      }
    });

    console.log(productData);

    setProduct(productArr);
  }, [productData]);

  return (
    <>
      <ProductSearch />
      {product}
    </>
  );
}
