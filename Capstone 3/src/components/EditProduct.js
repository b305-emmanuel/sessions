import { Button, Modal, Form } from "react-bootstrap";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";

export default function EditProduct({ product, fetchData }) {
  const [productId, setProductId] = useState("");

  const [showEdit, setShowEdit] = useState(false);

  // useState for our form (modal)
  const [productName, setProductName] = useState("");
  const [category, setCategory] = useState(``);
  const [stock, setStock] = useState(``);
  const [productDescription, setProductDescription] = useState("");
  const [price, setPrice] = useState("");

  // useEffect(() => {
  //   if (productId) {
  //     // Fetch product data when productId changes
  //     console.log(productId);
  //     fetch(`http://localhost:4000/products/${productId}`)
  //       .then((res) => res.json())
  //       .then((data) => {
  //         setProductName(data.productName);
  //         setCategory(data.category);
  //         setProductDescription(data.productDescription);
  //         setPrice(data.price);
  //         setStock(data.stock);
  //       });
  //   }
  // }, [productId]);

  const openEdit = (productId) => {
    setShowEdit(true);
    // setProductId(productId);

    fetch(`https://capstone-2-emg9.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductId(data._id);
        setProductName(data.productName);
        setCategory(data.category);
        setProductDescription(data.productDescription);
        setPrice(data.price);
        setStock(data.stock);
      });
  };

  const closeEdit = () => {
    setShowEdit(false);
    setProductName("");
    setCategory("");
    setProductDescription("");
    setPrice("");
    setStock("");
  };

  // function to save our update
  const editProduct = (e, productId) => {
    e.preventDefault();

    fetch(`http://localhost:4000/products/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productName: productName,
        category: category,
        productDescription: productDescription,
        price: price,
        stock: stock,
      }),
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error("Update Error");
        }
        //fetchData();
      })
      .then((data) => {
        console.log(data);

        // Refresh the data by re-fetching
        fetch(`http://localhost:4000/products/${productId}`)
          .then((res) => res.json())
          .then((data) => {
            console.log("Updated data:", data); // Add this line
            setProductName(data.productName);
            setCategory(data.category);
            setProductDescription(data.productDescription);
            setPrice(data.price);
            setStock(data.stock);
          });

        Swal.fire({
          title: "Update Success!",
          icon: "success",
          text: "Product Successfully Updated!",
        });
        fetchData();

        closeEdit();
      })
      .catch((error) => {
        console.error(error);

        Swal.fire({
          title: "Update Error!",
          icon: "error",
          text: "Please try again!",
        });
        fetchData();
      });
  };

  //   useEffect(() => {
  //     if (productId) {
  //       // Fetch product data when productId changes
  //       console.log(productId);
  //       fetch(`http://localhost:4000/products/${productId}`)
  //         .then((res) => res.json())
  //         .then((data) => {
  //           setProductName(data.productName);
  //           setCategory(data.category);
  //           setProductDescription(data.productDescription);
  //           setPrice(data.price);
  //           setStock(data.stock);
  //         });
  //     }
  //   }, [productId]);

  return (
    <>
      <Button
        variant="primary"
        size="sm"
        onClick={() => {
          openEdit(product);
        }}
      >
        Edit
      </Button>

      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={(e) => editProduct(e, productId)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                required
                value={productName}
                onChange={(e) => setProductName(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="category">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                required
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="productDescription">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                type="text"
                required
                value={productDescription}
                onChange={(e) => setProductDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                required
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="stock">
              <Form.Label>Stock</Form.Label>
              <Form.Control
                type="number"
                required
                value={stock}
                onChange={(e) => setStock(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
