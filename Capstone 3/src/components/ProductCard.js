import { Card, Button } from "react-bootstrap";

import { useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function ProductCard({ productProp }) {
  // Checks to see if the data was successfully passed
  //console.log(props);
  // Every component recieves information in a form of an object
  //console.log(typeof props);

  const { _id, productName, category, productDescription, price } = productProp;

  return (
    <>
      <Card>
        <Card.Body>
          <Card.Title>{category}</Card.Title>
          <Card.Title>{productName}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{productDescription}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
          <Link className="btn btn-primary" to={`/products/${_id}`}>
            Details
          </Link>
        </Card.Body>
      </Card>
    </>
  );
}

// Check if the ProductCard component is getting the correct prop types
ProductCard.propTypes = {
  product: PropTypes.shape({
    productName: PropTypes.string.isRequired,
    productDescription: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
