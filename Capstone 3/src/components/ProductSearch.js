import React, { useState } from "react";
import ProductCard from "./ProductCard";

const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch(
        "https://capstone-2-emg9.onrender.com/products/searchProduct",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ productName: searchQuery }),
        }
      );
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error("Error searching for product:", error);
    }
  };

  return (
    <div className="pt-5 container bg-p">
      <h2>Product Search</h2>
      <div className="form-group">
        <label htmlFor="productName">Product Name:</label>
        <input
          type="text"
          id="productName"
          className="form-control"
          value={searchQuery}
          onChange={(event) => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      {searchResults.length === 0 ? (
        <p>No product found</p>
      ) : (
        <ul>
          {searchResults.map((product) => (
            <div className="p-3 bg-success" key={product._id}>
              {/* => ProductCard */}
              <ProductCard productProp={product} />
            </div>
          ))}
        </ul>
      )}
    </div>
  );
};

export default ProductSearch;
