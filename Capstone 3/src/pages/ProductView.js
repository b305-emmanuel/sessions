import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function ProductView() {
  const { user } = useContext(UserContext);

  const { productId } = useParams();
  const navigate = useNavigate();

  const [productName, setProductName] = useState("");
  const [productDescription, setProductDescription] = useState("");
  const [quantity, setQuantity] = useState(0);
  const [price, setPrice] = useState(0);

  const enroll = () => {
    fetch(`https://capstone-2-emg9.onrender.com/cart/add-to-cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productName: productName,
        productDescription: productDescription,
        quantity: quantity,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Added to cart!",
            icon: "success",
            text: "You have successfully added this product to your cart",
          });

          navigate("/products");
        } else {
          Swal.fire({
            title: "Something Went Wrong!",
            icon: "error",
            text: "Please try again!",
          });
        }
      });
  };

  useEffect(() => {
    fetch(`https://capstone-2-emg9.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductName(data.productName);
        setProductDescription(data.productDescription);
        setPrice(data.price);
        setQuantity(1);
      });
  }, [productId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{productName}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{productDescription}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              {user.id !== null ? (
                <>
                  <Button variant="primary" onClick={() => enroll(productId)}>
                    Add to Cart
                  </Button>
                </>
              ) : (
                <Link className="btn btn-danger btn-block" to="/login">
                  Login to Enroll
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
