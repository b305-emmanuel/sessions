import { useState, useEffect } from "react";
import { Table } from "react-bootstrap";

import EditProduct from "../components/EditProduct";
import ArchiveProduct from "../components/ArchiveProduct";

export default function AdminView({ productData, fetchData }) {
  const [product, setProduct] = useState([]);

  useEffect(() => {
    const productArr = productData.map((product) => {
      return (
        <tr key={product._id}>
          <td>{product._id}</td>
          <td>{product.productName}</td>
          <td>{product.category}</td>
          <td>{product.productDescription}</td>
          <td>{product.price}</td>
          <td>{product.stock}</td>
          <td className={product.isActive ? "text-success" : "text-danger"}>
            {product.isActive ? "Available" : "Unavailable"}
          </td>
          <td>
            {" "}
            <EditProduct product={product._id} fetchData={fetchData} />{" "}
          </td>
          <td>
            <ArchiveProduct
              product={product._id}
              isActive={product.isActive}
              fetchData={fetchData}
            />
          </td>
        </tr>
      );
    });

    setProduct(productArr);
  }, [productData]);

  return (
    <>
      <h1 className="text-center my-4"> Admin Dashboard</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Category</th>
            <th>Description</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>

        <tbody>{product}</tbody>
      </Table>
    </>
  );
}
