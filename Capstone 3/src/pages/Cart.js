import React, { useState, useEffect } from "react";
import CartView from "../components/CartView";
import Swal from "sweetalert2";

export default function Cart() {
  const [cartItems, setCartItems] = useState([]);
  const [userName, setUserName] = useState("");
  const [selectedProducts, setSelectedProducts] = useState([]);

  const fetchData = () => {
    fetch("https://capstone-2-emg9.onrender.com/cart/viewCart", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setCartItems(data.items);
          setUserName(data.userName);
        } else {
          setCartItems(data.items);
        }
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  //Check box
  const handleChange = (productName) => {
    setSelectedProducts(productName);
    if (selectedProducts.includes(productName)) {
      setSelectedProducts(
        selectedProducts.filter((name) => name !== productName)
      );
    } else {
      setSelectedProducts([...selectedProducts, productName]);

      console.log(selectedProducts);
    }
  };

  // Update quantity on the Interface
  const updateQuantity = (productName, newQuantity) => {
    const token = localStorage.getItem("token");

    if (!token) {
      // Handle the case where the user is not authenticated
      console.log("User is not authenticated.");
      return;
    }

    const requestBody = {
      productName: productName,
      quantity: newQuantity,
    };

    fetch(`http://localhost:4000/cart/updateCart`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(requestBody),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          // Handle the case where the server returned an error
          console.log("Failed to update the cart.");
          return null;
        }
      })
      .then((data) => {
        if (data) {
          // If the update is successful, you can handle the updated cart data as needed
          console.log("Cart updated successfully:", data);
          // Perform any client-side updates here
          fetchData();
        }
      })
      .catch((error) => {
        // Handle any network or other errors here
        console.error("Error updating the cart:", error);
      });
  };

  const removeFromCart = function (productName) {
    const token = localStorage.getItem("token");

    if (!token) {
      // Handle the case where the user is not authenticated
      console.log("User is not authenticated.");
      return;
    }

    fetch(`http://localhost:4000/cart/remove-product`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productName,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          console.log(`Product Removed from Cart`);
          Swal.fire({
            title: "Item removed from cart!",
            icon: "success",
            text: "You have successfully removed this product from your cart",
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text: "There was a problem while removing the product from your cart",
          });
        }
      })
      .catch((error) => {
        // Handle any network or other errors here
        console.error("Error updating the cart:", error);
      });
  };

  const checkOut = function (productName, quantity) {
    const token = localStorage.getItem("token");

    if (!token) {
      // Handle the case where the user is not authenticated
      console.log("User is not authenticated.");
      return;
    }

    // Create an array of products to checkout
    const productsToCheckout = [
      {
        productName: productName,
        quantity: quantity, // Adjust the quantity as needed
      },
      // Add more products if needed
    ];

    fetch(`http://localhost:4000/cart/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ productsToCheckout }), // Send the array
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Checked out!!",
            icon: "success",
            text: "You have successfully checked out this product",
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Please try again",
            icon: "error",
            text: "there was an error",
          });
        }
      });
  };

  return (
    <>
      <CartView
        // => CartView
        cartItems={cartItems}
        // => CartView
        userName={userName}
        // => Quantity Updater
        updateQuantity={updateQuantity}
        // => CartView
        removeFromCart={removeFromCart}
        // => CartView
        checkOut={checkOut}
        //
        selectedProducts={selectedProducts}
        handleChange={handleChange}
      />
    </>
  );
}
