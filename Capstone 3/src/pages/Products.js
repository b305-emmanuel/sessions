import { useEffect, useState, useContext } from "react";

import UserContext from "../UserContext";
import UserView from "../components/UserView";
import AdminView from "./AdminView";

export default function Products() {
  const { user } = useContext(UserContext);

  const [product, setProduct] = useState([]);

  const fetchData = () => {
    fetch(`https://capstone-2-emg9.onrender.com/products/allProducts`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setProduct(data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {user.isAdmin === true ? (
        <>
          <AdminView productData={product} fetchData={fetchData} />
        </>
      ) : (
        <>
          <UserView productData={product} />
        </>
      )}
    </>
  );
}
