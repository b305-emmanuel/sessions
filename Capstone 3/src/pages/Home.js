import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home() {
  const data = {
    /* textual contents*/
    title: "Welcome to EZ Shop",
    content: "Shop from all over the World",
    /* buttons */
    destination: "/products",
    label: "Shop now!",
  };

  return (
    <>
      <Banner data={data} />
      <Highlights />
    </>
  );
}
