import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function AddProduct() {
  const navigate = useNavigate();

  const { user } = useContext(UserContext);

  //input states
  const [productName, setProductName] = useState("");
  const [category, setCategory] = useState(``);
  const [productDescription, setProductDescription] = useState("");
  const [price, setPrice] = useState("");
  const [stock, setStock] = useState(``);

  function createProduct(e) {
    //prevent submit event's default behavior
    e.preventDefault();

    let token = localStorage.getItem("token");
    console.log(token);

    fetch("https://capstone-2-emg9.onrender.com/products/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productName: productName,
        category: category,
        productDescription: productDescription,
        price: price,
        stock: stock,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        //data is the response of the api/server after it's been process as JS object through our res.json() method.
        console.log(data);

        if (data) {
          Swal.fire({
            icon: "success",
            title: "Product Added",
          });

          navigate("/products");
        } else {
          Swal.fire({
            icon: "error",
            title: "Unsuccessful Product Creation",
            text: data.message,
          });
        }
      });

    setProductName("");
    setProductDescription("");
    setCategory(``);
    setPrice(0);
    setStock(0);
  }

  return user.isAdmin === true ? (
    <>
      <h1 className="my-5 text-center">Add Product</h1>
      <Form onSubmit={(e) => createProduct(e)}>
        <Form.Group>
          <Form.Label>Product Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Name"
            required
            value={productName}
            onChange={(e) => {
              setProductName(e.target.value);
            }}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Category:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Category"
            required
            value={category}
            onChange={(e) => {
              setCategory(e.target.value);
            }}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Description:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Description"
            required
            value={productDescription}
            onChange={(e) => {
              setProductDescription(e.target.value);
            }}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Price:</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter Price"
            required
            value={price}
            onChange={(e) => {
              setPrice(e.target.value);
            }}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Product Quantity:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Stocks"
            required
            value={stock}
            onChange={(e) => {
              setStock(e.target.value);
            }}
          />
        </Form.Group>

        <Button variant="primary" type="submit" className="my-5">
          Submit
        </Button>
      </Form>
    </>
  ) : (
    <Navigate to="/products" />
  );
}
