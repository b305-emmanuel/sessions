Motivation for joining the boot camp:
My goal in enrolling in the boot camp is to become proficient in the foundational concepts of coding so I can be a more productive software engineer and developer in the future.


Work experience: 
My professional background is primarily in engineering, technology, and Artificial Intelligence. To increase the market and revenue of my clients, I concentrate mostly on creating business models, procedures, and platforms using low code and AI models. I also Manage and Lead Projects for clients. AI is the main core of my business. After receiving my BS in Industrial and Management Engineering, I pursued a career specializing in AI Engineering and Data Science. My current line of work includes an agency for artificial intelligence automation, SMMA, Media Buying, SAAS, AI services, and soon, AI training and certifications.  